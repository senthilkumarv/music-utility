FROM rustlang/rust:nightly as builder

WORKDIR /src
COPY Cargo.toml Cargo.lock /src/
COPY src /src/src
RUN apt-get update && apt-get install -y libavcodec-dev libavformat-dev libavfilter-dev libavdevice-dev libavutil-dev libavcodec-dev libavdevice-dev libavformat-dev libavfilter-dev libavresample-dev libswresample-dev libswscale-dev
RUN cargo build --release

FROM debian:stretch

WORKDIR /root/
COPY --from=builder /src/target/release/music-utility .
RUN apt-get update && apt-get install -y libavcodec-dev libavformat-dev libavfilter-dev libavdevice-dev libavutil-dev libavcodec-dev libavdevice-dev libavformat-dev libavfilter-dev libavresample-dev libswresample-dev libswscale-dev libssl-dev ca-certificates
ENV ROCKET_ENV=development
COPY Rocket.toml /root/
CMD "/root/music-utility"
