#![feature(plugin)]
#![plugin(rocket_codegen)]
#![feature(custom_derive)]

#[macro_use]
extern crate serde_derive;
extern crate rocket_contrib;
extern crate rocket;
extern crate ffmpeg;
extern crate reqwest;
extern crate uuid;

use rocket::request::Form;
use std::fs::File;
use std::io::copy;
use std::ops::Deref;
use rocket_contrib::Json;
use rocket::http::Status;
use std::env;
use std::fs;
use rocket::response::status::Custom;
use uuid::Uuid;

#[derive(FromForm)]
struct MusicDurationRequest {
  file_name: String,
}

#[derive(Serialize, Deserialize)]
struct MusicDurationSuccessResponse {
  file_name: String,
  duration_in_seconds: i64,
}

#[derive(Serialize, Deserialize)]
struct MusicDurationErrorResponse {
  file_name: String,
  reason: String,
}

#[post("/duration", data = "<request>")]
fn index(request: Form<MusicDurationRequest>) -> Result<Json<MusicDurationSuccessResponse>, Custom<String>> {
  let file_name = request.into_inner().file_name;
  let target = file_name.deref();
  match reqwest::get(target) {
    Ok(mut response) => {
      match save_to_file(&mut response) {
        Ok(tmp_file_name) => {
          let result = match ffmpeg::format::input(&tmp_file_name) {
            Ok(context) => Ok(Json(MusicDurationSuccessResponse {
              file_name: file_name.to_string(),
              duration_in_seconds: context.duration() / 1000000,
            })),
            Err(err) => Err(Custom(Status{ code: 500, reason: ""}, err.to_string()))
          };
          let _ = fs::remove_file(tmp_file_name);
          result
        }
        Err(err) => Err(Custom(Status{ code: 400, reason: ""}, err.to_string()))
      }
    }
    Err(err) => Err(Custom(Status{ code: 400, reason: ""}, err.to_string()))
  }
}

fn save_to_file(response: &mut reqwest::Response) -> Result<String, std::io::Error> {
  let tmp_dir = match env::current_dir() {
    Ok(dir) => dir,
    Err(err) => return Err(err)
  };
  let name = format!("{}", Uuid::new_v4());
  let temp_file_name = tmp_dir.join(name);
  let file_name_to_return = format!("{:?}", temp_file_name).replace("\"", "");
  let mut dest = match File::create(temp_file_name) {
    Ok(file) => file,
    Err(err) => return Err(err)
  };
  match copy(response, &mut dest) {
    Ok(_result) => Ok(file_name_to_return),
    Err(err) => Err(err)
  }
}

fn main() {
  ffmpeg::init().unwrap();
  rocket::ignite().mount("/", routes![index]).launch();
}